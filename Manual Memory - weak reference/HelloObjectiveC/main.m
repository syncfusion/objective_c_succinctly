//
//  main.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Ship.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
    
        Person *frank = [[Person alloc] init];
        Ship *discoveryOne = [[Ship alloc] init];
        
        frank.name = @"Frank";
        [discoveryOne setCaptain:frank];
        NSLog(@"%@", [discoveryOne captain].name);
        
        [frank release];
        
        // [discoveryOne captain] is now invalid
        NSLog(@"%@", [discoveryOne captain].name);
    
        [discoveryOne release];
    }
    
   
    return 0;
}

