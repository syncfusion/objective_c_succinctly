//
//  Ship.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Ship.h"

@implementation Ship{
    Person* _captain;
}

- (Person *)captain {
    return _captain;
}

// with ARC no special memory management code is needed
// the compiler figures out what we had to add manually
// earlier
- (void)setCaptain:(Person *)theCaptain {
    _captain = theCaptain;
}

@end
