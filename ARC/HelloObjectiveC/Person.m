//
//  Person.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Person.h"

@implementation Person
@synthesize name = _name;
// there is no specific need for dealloc either
@end
