//
//  main.m
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"
#import "Ship.h"

int main(int argc, const char * argv[])
{
    // with ARC no special memory management code is needed
    // the compiler figures out what we had to add manually
    // earlier
    @autoreleasepool {
    
        Person *frank = [[Person alloc] init];
        Ship *discoveryOne = [[Ship alloc] init];
        
        frank.name = @"Frank";
        [discoveryOne setCaptain:frank];
        NSLog(@"%@", [discoveryOne captain].name);
    }
    
   
    return 0;
}

