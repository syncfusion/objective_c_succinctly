//
//  Person.h
//  Selectors
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (copy) NSString *name;
@property (weak) Person *friend;
@property SEL action;

- (void)sayHello;
- (void)sayGoodbye;
- (void)coerceFriend;
@end
