//
//  Person.h
//  Categories
//
//  Created by Ryan Hodson on 11/8/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
@property (readonly) NSMutableArray* friends;
@property (copy) NSString *name;

- (void)sayHello;
- (void)sayGoodbye;
@end
