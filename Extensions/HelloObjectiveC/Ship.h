//
//  Ship.h
//  HelloObjectiveC
//
//  Created by Ryan Hodson on 11/7/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Person.h"

@interface Ship : NSObject

@property (strong, readonly) Person *captain;

- (id)initWithCaptain:(Person *)captain;

@end